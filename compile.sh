#!/bin/sh

cmake .
make

rm Makefile
rm cmake_install.cmake
rm CMakeCache.txt
rm -r CMakeFiles
