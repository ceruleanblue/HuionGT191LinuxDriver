/*
This is a free driver for the Huion Kamvas GT-191 running on linux.
Copyright (C) 2018  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Monitor.h"

#include <string.h>

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>

bool findMonitor_Crtc(Monitor* monitor, Display* display, XRRScreenResources* resources, const XRRCrtcInfo* info) {
	int index = 0;
	bool found = false;

	while ((index < info->noutput) && (!found)) {
		RROutput value = info->outputs[index++];

		XRROutputInfo* output = XRRGetOutputInfo(display, resources, value);

		if (output == NULL) {
			continue;
		}

		int properties_count = 0;

		Atom* properties = XRRListOutputProperties(display, value, &properties_count);

		if (properties != NULL) {
			for (int i = 0; i < properties_count; i++) {
				const char* name = XGetAtomName(display, properties[i]);

				if (strcmp(name, "EDID") != 0) {
					continue;
				}

				u_char* property;
				int actual_format;
				ulong item_count;
				ulong bytes_after;
				Atom actual_type;

				XRRGetOutputProperty(
						display,
						value,
						properties[i],
						0, 100, False, False,
						AnyPropertyType,
						&actual_type,
						&actual_format,
						&item_count,
						&bytes_after,
						&property
				);

				if ((actual_type == XA_INTEGER) &&
					(actual_format == 8) && (item_count >= 12) && ((property[0] | property[7]) == 0x00) &&
					((property[1] & property[2] & property[3] & property[4] & property[5] & property[6]) == 0xFF)) {
					const ushort vendorId = (ushort) property[8]  | ((ushort) property[9] << 8);
					const ushort productId = (ushort) property[10] | ((ushort) property[11] << 8);



					found |= ((monitor->vendorId == vendorId) && (monitor->productId == productId));
				}
			}
		}

		XRRFreeOutputInfo(output);
	}

	if (found) {
		monitor->x = info->x;
		monitor->y = info->y;

		monitor->width = info->width;
		monitor->height = info->height;
	}

	return found;
}

bool findMonitor_Screen(Monitor* monitor, Display* display, Screen* screen) {
	const int screen_number = XScreenNumberOfScreen(screen);
	const Window root = XRootWindow(display, screen_number);

	XRRScreenResources* resources = XRRGetScreenResources(display, root);

	if (resources == NULL) {
		return false;
	}

	int index = 0;
	bool found = false;

	while ((index < resources->ncrtc) && (!found)) {
		XRRCrtcInfo* info = XRRGetCrtcInfo(display, resources, resources->crtcs[index++]);

		if (info == NULL) {
			continue;
		}

		found = findMonitor_Crtc(monitor, display, resources, info);

		XRRFreeCrtcInfo(info);
	}

	XRRFreeScreenResources(resources);

	if (found) {
		monitor->screen_number = screen_number;

		monitor->screen_width = screen->width;
		monitor->screen_height = screen->height;
	}

	return found;
}

void findMonitor_Default(Monitor* monitor, Display* display, Screen* screen) {
	const int screen_number = XScreenNumberOfScreen(screen);
	const Window root = RootWindow(display, screen_number);

	XRRScreenResources* resources = XRRGetScreenResources(display, root);

	bool setupBounds = false;

	if (resources != NULL) {
		if (resources->crtcs > 0) {
			XRRCrtcInfo* info = XRRGetCrtcInfo(display, resources, resources->crtcs[0]);

			if (info != NULL) {
				monitor->x = info->x;
				monitor->y = info->y;

				monitor->width = info->width;
				monitor->height = info->height;

				setupBounds = true;

				XRRFreeCrtcInfo(info);
			}
		}

		XRRFreeScreenResources(resources);
	}

	monitor->screen_number = screen_number;

	monitor->screen_width = screen->width;
	monitor->screen_height = screen->height;

	if (!setupBounds) {
		monitor->x = 0;
		monitor->y = 0;

		monitor->width = monitor->screen_width;
		monitor->height = monitor->screen_height;
	}
}

bool findMonitor(Monitor* monitor) {
	Display* display = XOpenDisplay(NULL);

	if (display == NULL) {
		return false;
	}

	const int screen_count = XScreenCount(display);

	int screen_index = 0;
	bool found = false;

	while ((screen_index < screen_count) && (!found)) {
		Screen* screen = XScreenOfDisplay(display, screen_index++);

		if (screen != NULL) {
			found = findMonitor_Screen(monitor, display, screen);
		}
	}

	if (!found) {
		Screen* screen = XScreenOfDisplay(display, XDefaultScreen(display));

		if (screen != NULL) {
			findMonitor_Default(monitor, display, screen);
		}
	}

	XCloseDisplay(display);

	return found;
}

