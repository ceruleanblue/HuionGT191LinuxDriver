/*
This is a free driver for the Huion Kamvas GT-191 running on linux.
Copyright (C) 2018  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Calibration.h"

#include <stdio.h>

nnc_val nnc_funcSquare(nnc_val x) {
	return x*x;
}

nnc_val nnc_diffSquare(nnc_val x, nnc_val y) {
	return 2.0 * x;
}

const nnc_activate NNC_Square = { nnc_funcSquare, nnc_diffSquare };

void openCalibration(Calibration* calibration) {
	calibration->network = nnc_network_create();

	nnc_node* b0 = nnc_nodeBias(calibration->network);

	calibration->in_x = nnc_nodeInput(calibration->network, NNC_Identity);
	calibration->in_y = nnc_nodeInput(calibration->network, NNC_Identity);

	nnc_node* h1 = nnc_nodeHidden(calibration->network, NNC_Sum, NNC_Identity);
	nnc_node* h2 = nnc_nodeHidden(calibration->network, NNC_Sum, NNC_Identity);

	nnc_node* h3 = nnc_nodeHidden(calibration->network, NNC_Sum, NNC_Square);
	nnc_node* h4 = nnc_nodeHidden(calibration->network, NNC_Sum, NNC_Square);

	nnc_node* h5 = nnc_nodeHidden(calibration->network, NNC_Sum, NNC_Identity);
	nnc_node* h6 = nnc_nodeHidden(calibration->network, NNC_Sum, NNC_Identity);

	nnc_node* h7 = nnc_nodeHidden(calibration->network, NNC_Product, NNC_Identity);
	nnc_node* h8 = nnc_nodeHidden(calibration->network, NNC_Product, NNC_Identity);

	calibration->out_x = nnc_nodeOutput(calibration->network, NNC_Sum);
	calibration->out_y = nnc_nodeOutput(calibration->network, NNC_Sum);

	calibration->links[0] = nnc_link_add(b0, h1, -0.5);
	calibration->links[1] = nnc_link_add(calibration->in_x, h1, 1.0);
	calibration->links[2] = nnc_link_add(calibration->in_y, h1, 0.0);

	calibration->links[3] = nnc_link_add(b0, h2, -0.5);
	calibration->links[4] = nnc_link_add(calibration->in_x, h2, 0.0);
	calibration->links[5] = nnc_link_add(calibration->in_y, h2, 1.0);

	calibration->links[6] = nnc_link_add(b0, h3, 0.0);
	calibration->links[7] = nnc_link_add(h1, h3, 2.0);

	calibration->links[8] = nnc_link_add(b0, h4, 0.0);
	calibration->links[9] = nnc_link_add(h2, h4, 2.0);

	calibration->links[10] = nnc_link_add(b0, h5, -1.0);
	calibration->links[11] = nnc_link_add(h3, h5, 1.0);

	calibration->links[12] = nnc_link_add(b0, h6, -1.0);
	calibration->links[13] = nnc_link_add(h4, h6, 1.0);

	calibration->links[14] = nnc_link_add(h1, h7, 2.0);
	calibration->links[15] = nnc_link_add(h6, h7, -1.0);

	calibration->links[16] = nnc_link_add(h2, h8, 2.0);
	calibration->links[17] = nnc_link_add(h5, h8, -1.0);

	calibration->links[18] = nnc_link_add(b0, calibration->out_x, 0.5);
	calibration->links[19] = nnc_link_add(h1, calibration->out_x, 1.002);
	calibration->links[20] = nnc_link_add(h2, calibration->out_x, 0.0);
	calibration->links[21] = nnc_link_add(h7, calibration->out_x, 0.001);
	calibration->links[22] = nnc_link_add(h8, calibration->out_x, 0.0);

	calibration->links[23] = nnc_link_add(b0, calibration->out_y, 0.502);
	calibration->links[24] = nnc_link_add(h1, calibration->out_y, 0.0);
	calibration->links[25] = nnc_link_add(h2, calibration->out_y, 1.005);
	calibration->links[26] = nnc_link_add(h7, calibration->out_y, 0.0);
	calibration->links[27] = nnc_link_add(h8, calibration->out_y, 0.0005);

	FILE* f = fopen("calibration.txt", "r");

	if (f != NULL) {
		for (int i = 0; i < CALIBRATION_LINKS; i++) {
			fscanf(f, "%Lf ", &(calibration->links[i]->factor));
		}

		fscanf(f, "\n");
		fclose(f);
	}

	nnc_setup(calibration->network);

	calibration->pairs_size = 1;
	calibration->pairs_count = 0;
	calibration->pairs = (CalPair*) malloc(sizeof(CalPair));
}

void readCalibration(Calibration* calibration, nnc_val x, nnc_val y, nnc_val* cx, nnc_val* cy) {
	nnc_node_set(calibration->in_x, x);
	nnc_node_set(calibration->in_y, y);

	nnc_update(calibration->network);

	*cx = nnc_node_get(calibration->out_x);
	*cy = nnc_node_get(calibration->out_y);
}

#define EPSILON 0.0001

void writeCalibration(Calibration* calibration, nnc_val x, nnc_val y, nnc_val cx, nnc_val cy) {
	nnc_node_set(calibration->in_x, x);
	nnc_node_set(calibration->in_y, y);

	nnc_node_expect(calibration->out_x, cx);
	nnc_node_expect(calibration->out_y, cy);

	nnc_update(calibration->network);
	nnc_upgrade(calibration->network, EPSILON);
}

void resetPairsOfCalibration(Calibration* calibration) {
	calibration->pairs_size = 1;
	calibration->pairs_count = 0;

	calibration->pairs = (CalPair*) realloc((void*) calibration->pairs, sizeof(CalPair) * calibration->pairs_size);
}

void addPairToCalibration(Calibration* calibration, nnc_val x, nnc_val y, nnc_val cx, nnc_val cy) {
	if (calibration->pairs_count + 1 >= calibration->pairs_size) {
		calibration->pairs_size <<= 1;
		calibration->pairs = (CalPair*) realloc((void*) calibration->pairs, sizeof(CalPair) * calibration->pairs_size);
	}

	calibration->pairs[calibration->pairs_count].x1 = x;
	calibration->pairs[calibration->pairs_count].y1 = y;
	calibration->pairs[calibration->pairs_count].x2 = cx;
	calibration->pairs[calibration->pairs_count].y2 = cy;

	calibration->pairs_count++;
}

void writePairsToCalibration(Calibration* calibration) {
	for (uint j = 0; j < 10; j++) {
		for (uint i = 0; i < calibration->pairs_count; i++) {
			writeCalibration(
					calibration,
					calibration->pairs[i].x1,
					calibration->pairs[i].y1,
					calibration->pairs[i].x2,
					calibration->pairs[i].y2
			);
		}
	}
}

void closeCalibration(Calibration* calibration) {
	if (calibration->network != NULL) {
		FILE* f = fopen("calibration.txt", "w");

		if (f != NULL) {
			for (int i = 0; i < CALIBRATION_LINKS; i++) {
				fprintf(f, "%Lf ", calibration->links[i]->factor);
			}

			fprintf(f, "\n");
			fflush(f);
			fclose(f);
		}

		nnc_network_destroy(calibration->network);

		calibration->network = NULL;

		calibration->in_x = NULL;
		calibration->in_y = NULL;

		calibration->out_x = NULL;
		calibration->out_y = NULL;

		free((void*) calibration->pairs);

		calibration->pairs = NULL;
	}
}