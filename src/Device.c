/*
This is a free driver for the Huion Kamvas GT-191 running on linux.
Copyright (C) 2018  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Device.h"

#include <string.h>
#include <libusb-1.0/libusb.h>

void findDevice_Params1(Device* device, const u_char* buf) {
	int off = 2;

	device->maxX = (buf[off++] & 0xFF) | ((buf[off++] & 0xFF) << 8);
	device->maxY = (buf[off++] & 0xFF) | ((buf[off++] & 0xFF) << 8);
	device->maxPressure = (buf[off++] & 0xFF) | ((buf[off++] & 0xFF) << 8);
	device->resolution = (buf[off++] & 0xFF) | ((buf[off++] & 0xFF) << 8);
}

void findDevice_Params2(Device* device, const u_char* buf) {
	int off = 2;

	device->maxX = (buf[off++] & 0xFF) | ((buf[off++] & 0xFF) << 8) | ((buf[off++] & 0xFF) << 16);
	device->maxY = (buf[off++] & 0xFF) | ((buf[off++] & 0xFF) << 8) | ((buf[off++] & 0xFF) << 16);
	device->maxPressure = (buf[off++] & 0xFF) | ((buf[off++] & 0xFF) << 8);
	device->resolution = (buf[off++] & 0xFF) | ((buf[off++] & 0xFF) << 8);
}

bool findDevice(Device* device) {
	libusb_context** context = (libusb_context**) &(device->context);

	if (libusb_init(context) < 0) {
		return false;
	}

	libusb_device_handle* handle = libusb_open_device_with_vid_pid(*context, device->vendorId, device->productId);

	bool found = (handle != NULL);

	if (found) {
		u_char buf [256];
		int len;

		char params = 0;

		len = libusb_get_string_descriptor(handle, 0x64, 0x0409, buf, 255);

		if (len < 10) {
			len = libusb_get_string_descriptor(handle, 0xC8, 0x0409, buf, 255);

			if (len >= 12) {
				params = 2;
			}
		} else {
			params = 1;
		}

		switch (params) {
			case 1:
				findDevice_Params1(device, buf);
				break;
			case 2:
				findDevice_Params2(device, buf);
				break;
			default:
				found = false;
				break;
		}

		len = libusb_get_string_descriptor(handle, 0xC9, 0x0409, buf, 255);

		if (len >= 2) {
			len = (len - 2) / 2;

			if (len >= UINPUT_MAX_NAME_SIZE - DEV_EXTRA_PEN_SUFFIX_LEN) {
				len = UINPUT_MAX_NAME_SIZE - 1 - DEV_EXTRA_PEN_SUFFIX_LEN;
			}

			if (len > 0) {
				ushort unicode;

				for (int i = 0; i < len; i++) {
					unicode = buf[2 + i * 2] | (buf[3 + i * 2] << 8);

					device->name[i] = (char) (
							(unicode >= 0x20 && unicode <= 0x7E) ? (unicode & 0xFF) : '?'
					);
				}
			}
			
			strncpy(device->name + len, DEV_EXTRA_PEN_SUFFIX, DEV_EXTRA_PEN_SUFFIX_LEN);

			device->name[len + DEV_EXTRA_PEN_SUFFIX_LEN] = '\0';
		}

		libusb_close(handle);
	}

	libusb_exit(*context);

	return found;
}

bool openDevice(Device* device) {
	libusb_context** context = (libusb_context**) &(device->context);

	if (libusb_init(context) < 0) {
		return false;
	}

	libusb_device_handle* handle = libusb_open_device_with_vid_pid(*context, device->vendorId, device->productId);

	device->handle = NULL;

	if (handle != NULL) {
		struct libusb_device_descriptor desc;

		libusb_device* dev = libusb_get_device(handle);

		libusb_get_device_descriptor(dev, &desc);

		for (int i = 0; i < desc.bNumConfigurations; i++) {
			struct libusb_config_descriptor* config;

			if ((libusb_get_config_descriptor(libusb_get_device(handle), 0, &config) == 0)) {
				device->address = config->interface[0].altsetting[0].endpoint[0].bEndpointAddress;
			}
		}

		if (libusb_kernel_driver_active(handle, 0)) {
			libusb_detach_kernel_driver(handle, 0);
		}

		if (libusb_claim_interface(handle, 0) == 0) {
			device->handle = handle;
		}
	}

	return (device->handle != NULL);
}

int readDevice(Device* device, Event* event) {
	if (device->handle == NULL) {
		return -1;
	}

	static u_char buf [MAX_INPUT + 1];

	memset(buf, 0, sizeof(u_char) * (MAX_INPUT + 1));

	int len = 0;
	int check = libusb_bulk_transfer(device->handle, device->address, buf, MAX_INPUT, &len, 10);

	if (check != 0) {
		switch (check) {
			case LIBUSB_ERROR_TIMEOUT:
				return 0;
			case LIBUSB_ERROR_PIPE:
				return -3;
			case LIBUSB_ERROR_OVERFLOW:
				return -2;
			case LIBUSB_ERROR_NO_DEVICE:
				return -1;
			default:
				return -4;
		}
	} else
	if (len >= 10) {
		event->action = buf[1];

		event->x = (buf[2] & 0xFF) | ((buf[3] & 0xFF) << 8) | ((buf[8] & 0xFF) << 16);
		event->y = (buf[4] & 0xFF) | ((buf[5] & 0xFF) << 8) | ((buf[9] & 0xFF) << 16);

		event->pressure = (buf[6] & 0xFF) | ((buf[7] & 0xFF) << 8);

		return 1;
	} else {
		return 0;
	}
}

bool closeDevice(Device* device) {
	if (device->handle == NULL) {
		return false;
	}

	libusb_device_handle* handle = (libusb_device_handle*) device->handle;

	libusb_release_interface(handle, 0);

	libusb_close(handle);

	libusb_exit((libusb_context*) device->context);

	return true;
}
