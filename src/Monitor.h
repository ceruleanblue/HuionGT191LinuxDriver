#pragma once

/*
This is a free driver for the Huion Kamvas GT-191 running on linux.
Copyright (C) 2018  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdbool.h>
#include <stdlib.h>

typedef struct monitor_t {
	ushort vendorId;
	ushort productId;

	int screen_number;

	int screen_width;
	int screen_height;

	int x;
	int y;

	int width;
	int height;
} Monitor;

bool findMonitor(Monitor* monitor);
