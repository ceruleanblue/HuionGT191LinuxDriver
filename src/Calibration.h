#pragma once

/*
This is a free driver for the Huion Kamvas GT-191 running on linux.
Copyright (C) 2018  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Device.h"

#include "nnc.h"

typedef struct cal_pair_t {
	nnc_val x1;
	nnc_val y1;

	nnc_val x2;
	nnc_val y2;
} CalPair;

#define CALIBRATION_LINKS 28

typedef struct calibration_t {
	nnc_network* network;

	nnc_node* in_x;
	nnc_node* in_y;

	nnc_node* out_x;
	nnc_node* out_y;

	nnc_link* links [CALIBRATION_LINKS];

	uint pairs_size;
	uint pairs_count;
	CalPair* pairs;
} Calibration;

void openCalibration(Calibration* calibration);

void readCalibration(Calibration* calibration, nnc_val x, nnc_val y, nnc_val* cx, nnc_val* cy);

void writeCalibration(Calibration* calibration, nnc_val x, nnc_val y, nnc_val cx, nnc_val cy);

void resetPairsOfCalibration(Calibration* calibration);

void addPairToCalibration(Calibration* calibration, nnc_val x, nnc_val y, nnc_val cx, nnc_val cy);

void writePairsToCalibration(Calibration* calibration);

void closeCalibration(Calibration* calibration);
