#!/bin/sh
if [ ! -d nnc ]; then
	git clone https://gitlab.com/TheJackiMonster/nnc.git
else
	cd nnc

	git pull

	cd ..
fi

cd src

if [ ! -h nnc.h ]; then
	ln -s ../nnc/src/nnc.h
fi

cd ..
