#!/bin/bash
SUDO=''

if (( $EUID != 0 )); then
	SUDO='sudo'
fi

UDEV_RULES_DIR="/etc/udev/rules.d"
RULE_NAME="huion-gt191.rules"

XORG_CONF_DIR="/etc/X11/xorg.conf.d"
CONF_NAME="huion-tablet.conf"

if [[ $(ls $UDEV_RULES_DIR | grep $RULE_NAME) ]]; then
	$SUDO rm $UDEV_RULES_DIR/*-$RULE_NAME

	echo "Rules for udev are uninstalled now..."
fi

if [[ $(ls $XORG_CONF_DIR | grep $CONF_NAME) ]]; then
	$SUDO rm $XORG_CONF_DIR/*-$CONF_NAME

	echo "Configuration for xorg is uninstalled now..."
fi

$SUDO udevadm control --reload
$SUDO udevadm trigger

echo "Uninstallation completed..."

