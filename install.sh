#!/bin/bash
SUDO=''

if (( $EUID != 0 )); then
	SUDO='sudo'
fi

UDEV_RULES_DIR="/etc/udev/rules.d"
RULE_NAME="huion-gt191.rules"

XORG_CONF_DIR="/etc/X11/xorg.conf.d"
CONF_NAME="huion-tablet.conf"

if [[ $(ls $UDEV_RULES_DIR | grep $RULE_NAME) ]]; then
	echo "Rules for udev are already installed..."
else
	LAST_ID=$(ls $UDEV_RULES_DIR | sed -e "s/-.*\.rules//" | sort -r | sed -n 1p)
	NEW_ID=$(($LAST_ID+1))
	
	while [[ ${#NEW_ID} -lt 2 ]] ; do
    	NEW_ID="0${NEW_ID}"
	done
	
	$SUDO cp resources/$RULE_NAME $UDEV_RULES_DIR/$NEW_ID-$RULE_NAME
	
	echo "Rules for udev are installed now..."
fi

if [[ $(ls $XORG_CONF_DIR | grep $CONF_NAME) ]]; then
	echo "Configuration for xorg is already installed..."
else
	LAST_ID=$(ls $XORG_CONF_DIR | sed -e "s/-.*\.conf//" | sort -r | sed -n 1p)
	NEW_ID=$(($LAST_ID+1))

	while [[ ${#NEW_ID} -lt 2 ]] ; do
    	NEW_ID="0${NEW_ID}"
	done
	
	$SUDO cp resources/$CONF_NAME $XORG_CONF_DIR/$NEW_ID-$CONF_NAME
	
	echo "Configuration for xorg is installed now..."
fi

$SUDO udevadm control --reload
$SUDO udevadm trigger

echo "Installation completed..."

